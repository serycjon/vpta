# -*- origami-fold-style: triple-braces; coding: utf-8; -*-
import os
import sys
import argparse
from pathlib import Path
import logging
import gzip
import pickle
from ipdb import iex
logger = logging.getLogger(__name__)


def parse_arguments():
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', '--verbose', help='', action='store_true')
    parser.add_argument('--path', help='paths to annotation pickles', nargs='+',
                        type=Path, required=True)
    parser.add_argument('--export', help='output path (annot.pklz)', type=Path)

    args = parser.parse_args()

    format = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    lvl = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=lvl, format=format)
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    return args


@iex
def run(args):
    all_annotations = []
    for path in args.path:
        with gzip.open(path, 'rb') as fin:
            annotations = pickle.load(fin)
        all_annotations.append(annotations)

    merged = all_annotations[0]
    for annotations in all_annotations[1:]:
        for video_name in annotations.keys():
            video_annotations = annotations[video_name]
            if video_name not in merged:
                merged[video_name] = video_annotations
                continue
            for query_i in video_annotations.keys():
                if query_i not in merged[video_name]:
                    merged[video_name][query_i] = video_annotations[query_i]
                else:
                    merged[video_name][query_i].update(video_annotations[query_i])
                    logger.info(f'Updating {video_name} q{query_i} with new GT.')
    if args.export is not None:
        args.export.parent.mkdir(parents=True, exist_ok=True)
        with gzip.open(args.export, 'wb') as fout:
            pickle.dump(merged, fout)
        
    return merged


def main():
    args = parse_arguments()
    return run(args)


if __name__ == '__main__':
    results = main()
