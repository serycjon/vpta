# -*- origami-fold-style: triple-braces; coding: utf-8; -*-
import os
import sys
import argparse
from pathlib import Path
import json
import logging
from requests.exceptions import HTTPError

from vpta import io
from vpta.vis import GUI

logger = logging.getLogger(__name__)

with open('secrets.json', 'r') as fin:
    secrets = json.load(fin)



def parse_arguments():
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     fromfile_prefix_chars='@')
    parser.add_argument('-v', '--verbose', help='', action='store_true')
    parser.add_argument('--task', help='task name', required=True)
    parser.add_argument('--update', help='update task', action='store_true')
    parser.add_argument('--base_url', help='base url', default=secrets['base_url'])
    parser.add_argument('--mode', help='', choices=['annotate', 'create'], default='annotate')
    parser.add_argument('--video', help='skip directly to video id', type=int, default=0)


    args = parser.parse_args()

    format = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    lvl = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=lvl, format=format)
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    return args


def run(args):
    try:
        task_remote = f'{args.base_url}/tasks/{args.task}.json'
        task_local_dir = Path('tasks') / 'tasks' / args.task
        task_local_dir.mkdir(parents=True, exist_ok=True)
        task_local = task_local_dir / 'task.json'
        if not task_local.exists() or args.update:
            io.download(task_remote, task_local)
    except HTTPError:
        logger.error(f"Could not download the task from {task_remote}")
        task_local.unlink(missing_ok=True)
        raise

    with (task_local).open('r') as fin:
        task = json.load(fin)

    try:
        io.download_task_videos(task, args.base_url, Path('tasks') / 'videos')
    except HTTPError:
        logger.error("Could not download the task video(/s)")
        raise

    backup_upload_url = f'{args.base_url}/upload_backup.php'
    GUI(task_name=args.task,start_video=args.video, task=task, backup_upload_url=backup_upload_url,
        mode=args.mode)

    return 0




def main():
    args = parse_arguments()
    return run(args)


if __name__ == '__main__':
    sys.exit(main())
