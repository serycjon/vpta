# -*- origami-fold-style: triple-braces; coding: utf-8; -*-
import sys
import argparse
from pathlib import Path
import json
import logging
import gzip
import pickle
import numpy as np
import cv2
from ipdb import iex
import vpta.io as io
from vpta.vis import draw_text, crossout, crosshair, imshow, shorten_text, OverlayBackground

logger = logging.getLogger(__name__)

with open('secrets.json', 'r') as fin:
    secrets = json.load(fin)


def parse_arguments():
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     fromfile_prefix_chars='@')
    parser.add_argument('-v', '--verbose', help='', action='store_true')
    parser.add_argument('--task', help='task name', required=True)
    parser.add_argument('annots', help='paths to annot pickles', nargs='+', type=Path, metavar='annot')
    parser.add_argument('-V', '--video', help='jump to video', type=int, default=1)
    parser.add_argument('-Q', '--query', help='jump to query', type=int, default=1)

    args = parser.parse_args()

    format = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    lvl = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=lvl, format=format)
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    return args


def run(args):
    task_local_dir = Path('tasks') / 'tasks' / args.task
    task_local = task_local_dir / 'task.json'
    assert task_local.exists()
    with (task_local).open('r') as fin:
        task = json.load(fin)

    CompGUI(task, args.annots, args.video, args.query)

    return 0


class CompGUI(object):
    def __init__(self, task, annot_paths, video, query):
        self.task = task
        self.all_annotations = {}

        self.N_videos = None
        self.annot_names = annot_paths
        for annot_path in annot_paths:
            with gzip.open(str(annot_path), 'rb') as fin:
                self.all_annotations[annot_path] = pickle.load(fin)
                N_annot_videos = len(self.all_annotations[annot_path])
                if self.N_videos is None:
                    self.N_videos = N_annot_videos
                else:
                    if N_annot_videos != self.N_videos:
                        logger.warning('number of annotated videos not consistent!'
                                       f' ({N_annot_videos} vs {self.N_videos})')

        self.dragged = False

        self.video_i = video - 1
        self.query_i = query - 1
        self.frame_i = 0

        self.frames = None
        self.hud_hidden = False
        self.next_query(0)

        annot_colors = [(0, 0, 255), (0, 255, 0)]

        while True:
            current_vis = self.frames[self.frame_i].copy()

            video_name = self.task[self.video_i]['video']
            all_vis = []
            for annot_i, annot_name in enumerate(self.annot_names):
                annot_vis = self.frames[self.frame_i].copy()
                annot = self.all_annotations[annot_name][video_name][self.query_i]
                annot_color = annot_colors[annot_i]

                visibility_text = "visible: "
                visibility = self.is_currently_visible(annot)
                if visibility is None:
                    visibility_text += '???'
                elif visibility:
                    visibility_text += 'yes'
                else:
                    visibility_text += 'no'

                if not self.hud_hidden:
                    if visibility is False:
                        annot_vis = crossout(annot_vis, color=annot_color)
                    elif visibility is None:
                        annot_vis = draw_text(annot_vis, '???', size=8, thickness=8, pos='center',
                                              color=annot_color, bg=False, fit_in=True)
                    elif self.frame_i in annot and annot[self.frame_i].get("x"):
                        ctype = 'square' if annot[self.frame_i].get('track_assist', False) else 'circle'
                        if annot[self.frame_i].get('lerp_assist', False):
                            ctype = 'square_rot'
                        annot_vis = crosshair(annot_vis, (annot[self.frame_i]["x"],
                                                          annot[self.frame_i]["y"]),
                                              color=annot_color, ctype=ctype)
                    if self.frame_i in annot:
                        annot_vis = draw_text(annot_vis, 'key', size=2, thickness=2, pos='tr', color=annot_color)
                    frame_i_text = f'#{self.frame_i + 1}/{len(self.frames)} '
                    annot_vis = draw_text(annot_vis, frame_i_text + visibility_text, size=2, thickness=2, pos='tl',
                                          color=annot_color)
                all_vis.append(annot_vis)

            all_vis = np.array(all_vis, dtype=np.float32)
            current_vis = np.round(np.amax(all_vis, axis=0)).astype(np.uint8)

            # compute EPE of first two annotations, also convert to 256x256 resolution
            annot_a = self.all_annotations[self.annot_names[0]][video_name][self.query_i]
            annot_b = self.all_annotations[self.annot_names[1]][video_name][self.query_i]
            if self.frame_i in annot_a and annot_a[self.frame_i].get("x") and \
               self.frame_i in annot_b and annot_b[self.frame_i].get("x"):
                pt_a = np.array([annot_a[self.frame_i]["x"], annot_a[self.frame_i]["y"]]) - 0.5
                pt_b = np.array([annot_b[self.frame_i]["x"], annot_b[self.frame_i]["y"]]) - 0.5
                EPE = np.sqrt(np.sum(np.square(pt_a - pt_b)))

                shape_WH = np.array(self.frames[self.frame_i].shape[:2][::-1])
                pt_a_256 = pt_a * (256 / shape_WH)
                pt_b_256 = pt_b * (256 / shape_WH)

                EPE_256 = np.sqrt(np.sum(np.square(pt_a_256 - pt_b_256)))

                err_pts = np.sum([EPE_256 >= thr for thr in [1, 2, 4, 8, 16]])
                EPE_txt = f'EPE {EPE:>5.1f} | EPE256 {EPE_256:>4.1f} | error pts {err_pts}'
                current_vis = draw_text(current_vis, EPE_txt, size=2, thickness=2, pos='bl')

            imshow("cv: current", current_vis)
            c = cv2.waitKey(20)
            if c == ord('q'):
                break
            elif c in [ord('h'), 81, ord('n')]:  # left
                self.frame_i = max(0, self.frame_i - 1)
            elif c in [ord('l'), 83, ord('m')]:  # right
                self.frame_i = min(len(self.frames) - 1, self.frame_i + 1)
            elif c == ord('+'):  # next query
                self.next_query(+1)
            elif c == ord('-'):  # prev query
                self.next_query(-1)
            elif c == ord('x'):
                self.hud_hidden = not self.hud_hidden
                self.next_query(0)  # update images
            elif c >= 0:
                print(f"c: {c}")

    @iex
    def handler(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONUP:
            logger.debug(f"(x, y): {(x, y)}")
            on_query_frame = self.frame_i == self.queries[self.query_i]["frame_i"]
            # on_query_frame = False
            if self.dragged:
                return
        elif event == cv2.EVENT_RBUTTONDOWN:
            pass
        elif event == cv2.EVENT_MOUSEMOVE:
            self.dragged = True
        elif event == cv2.EVENT_LBUTTONDOWN:
            self.dragged = False

    def write_task(self):
        path = Path('tasks') / 'tasks' / self.task_name / 'task.json'
        with path.open('w') as fout:
            json.dump(self.task, fout, indent=2, sort_keys=True)

    def is_currently_visible(self, annot):
        visible = None
        for i in range(0, self.frame_i + 1):
            if (i in annot) and ("not_visible" in annot[i]):
                current_not_vis = annot[i]["not_visible"]
                if current_not_vis is None:
                    visible = None
                elif current_not_vis:
                    visible = False
                else:
                    visible = True

        return visible

    def next_query(self, delta=1):
        query_i = self.query_i + delta
        video_i = self.video_i
        video_changed = False
        if query_i < 0:
            # prev video
            video_i -= 1
            video_changed = True
            if video_i < 0:
                return
            query_i = len(self.task[video_i]['queries']) - 1
        elif (query_i >= len(self.task[self.video_i]['queries']) and
              len(self.task[self.video_i]['queries']) > 0):
            # next video
            video_changed = True
            video_i += 1
            if video_i >= len(self.task):
                return
            query_i = 0

        # load the appropriate query / video
        self.video_i = video_i
        self.query_i = query_i
        if video_changed or self.frames is None:
            video_path = Path('tasks') / 'videos' / self.task[self.video_i]['video']
            self.frames = [frame for frame in io.get_video_frames(video_path)]
            assert len(self.frames) > 0, "Loaded 0 video frames :("

        self.queries = self.task[self.video_i]['queries']
        query = self.queries[self.query_i]
        # template_vis = to_gray_3ch(self.frames[query["frame_i"]])
        template_vis = self.frames[query["frame_i"]].copy()
        if not self.hud_hidden:
            template_vis = crosshair(template_vis, (query['x'], query['y']), (0, 0, 255))
            template_str = (f'{shorten_text(self.task[self.video_i]["video"], 12, keep="right")}: '
                            f'q: {self.query_i + 1}/{len(self.queries)} v: {self.video_i + 1}/{len(self.task)} '
                            f'#{query["frame_i"]+1}')
            template_vis = draw_text(template_vis, template_str, size=2, thickness=2, pos='tl')

        imshow("cv: template", template_vis)
        if delta != 0:
            self.frame_i = query["frame_i"]
        current = self.frames[self.frame_i].copy()
        imshow("cv: current", current)
        cv2.setMouseCallback("cv: current", self.handler)

        video_name = self.task[self.video_i]['video']
        if video_name not in self.all_annotations[self.annot_names[0]]:
            self.all_annotations[self.annot_names[0]][video_name] = {}
        self.annot = self.all_annotations[self.annot_names[0]][video_name][query_i]


@iex
def main():
    args = parse_arguments()
    return run(args)


if __name__ == '__main__':
    sys.exit(main())
