# -*- origami-fold-style: triple-braces; coding: utf-8; -*-
import os
import sys
import argparse
from pathlib import Path
import logging
import gzip
import pickle
import json
import numpy as np
import einops
from ipdb import iex
from vpta import io
logger = logging.getLogger(__name__)


def parse_arguments():
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', '--verbose', help='', action='store_true')
    parser.add_argument('task', help='input task names', nargs='+')
    parser.add_argument('--out', help='output task name', required=True)
    parser.add_argument('--export_tapvid_dataset_from', help='path to original tapvid GT pickle', type=Path)

    args = parser.parse_args()

    format = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    lvl = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=lvl, format=format)
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    return args


def run(args):
    annotations = {}
    all_task_defs = []
    for task_name in args.task:
        print(task_name)
        annot_path = Path('tasks') / 'tasks' / task_name / 'annot.pklz'
        with gzip.open(str(annot_path), 'rb') as fin:
            all_annotations = pickle.load(fin)
        annotations.update(all_annotations)

        task_path = Path('tasks') / 'tasks' / task_name / 'task.json'
        with task_path.open('r') as fin:
            task = json.load(fin)
        all_task_defs += task

    out_dir = Path('tasks') / 'tasks' / args.out
    out_dir.mkdir(parents=True, exist_ok=True)
    with gzip.open(str(out_dir / 'annot.pklz'), 'wb') as fout:
        pickle.dump(annotations, fout)

    with open(str(out_dir / 'task.json'), 'w') as fout:
        json.dump(all_task_defs, fout, indent=2, sort_keys=True)

    if args.export_tapvid_dataset_from is not None:
        with open(args.export_tapvid_dataset_from, 'rb') as fin:
            data = pickle.load(fin)

        # data.keys(): sequence names
        # data[seq].keys(): ['points', 'occluded', 'video']
        # data[seq]['points']: (N_queries N_frames xynorm)
        # data[seq]['occluded']: (N_queries N_frames)
        # data[seq]['video']: (N_frames H W C)

    for seq_name in annotations.keys():
        N_queries = data[seq_name]['points'].shape[0]
        vid_N_frames, vid_H, vid_W = data[seq_name]['video'].shape[:3]
        video_path = Path('tasks') / 'videos' / seq_name
        N_frames = 0
        for frame in io.get_video_frames(video_path):
            N_frames += 1
        assert vid_N_frames == N_frames
        H, W = frame.shape[:2]
        pred_occluded = np.zeros((N_queries, N_frames))
        xy = 2
        pred_tracks = np.zeros((N_queries, N_frames, xy))

        for query_i in range(N_queries):
            for frame_i in range(N_frames):
                frame_annot = annotations[seq_name][query_i].get(frame_i, {'not_visible': True})
                if frame_annot['not_visible']:
                    pred_occluded[query_i, frame_i] = 1
                elif frame_annot['not_visible'] is False:
                    pred_tracks[query_i, frame_i, :] = (frame_annot['x'], frame_annot['y'])

        # already scaled (?)
        scale = einops.rearrange(np.array([1.0 / W, 1.0 / H]), 'xy -> 1 1 xy')
        pred_tracks *= scale
        assert np.all(pred_tracks <= 1)
        assert np.all(pred_tracks >= 0)

        data[seq_name]['points'] = pred_tracks.astype(np.float32)
        data[seq_name]['occluded'] = pred_occluded > 0
        data[seq_name]['occluded'] = data[seq_name]['occluded'].astype(np.bool_)
    with open(str(out_dir / 'dataset.pkl'), 'wb') as fout:
        pickle.dump(data, fout)

    return 0

@iex
def main():
    args = parse_arguments()
    return run(args)


if __name__ == '__main__':
    sys.exit(main())
