from ipdb import iex
from collections import defaultdict
import cv2
from pathlib import Path
from vpta import io
import datetime
import pickle
import gzip
import socket
from copy import copy
import json
import tqdm
import logging
logger = logging.getLogger(__name__)


class GUI(object):
    def __init__(self, task_name, task, backup_upload_url, mode='annotate',start_video=0):
        self.zoom_x = None
        self.zoom_y = None

        self.mode = mode
        self.task_name = task_name
        save_path = Path('tasks') / 'tasks' / self.task_name / 'annot.pklz'
        if save_path.exists():
            with gzip.open(str(save_path), 'rb') as fin:
                self.all_annotations = pickle.load(fin)
        else:
            self.all_annotations = {}
        self.task = task

        self.upload_url = backup_upload_url
        self.previous_backup = datetime.datetime.min
        self.previous_online_backup = datetime.datetime.min

        self.N_videos = len(task)

        self.dragged = False

        

        self.video_i = start_video -1
        self.query_i = 0
        self.frame_i = 0

        self.frames = None
        self.hud_hidden = False
        self.next_query(0)

        while True:
            visibility_text = "visible: "
            visibility = self.is_currently_visible()
            if visibility is None:
                visibility_text += '???'
            elif visibility:
                visibility_text += 'yes'
            else:
                visibility_text += 'no'

            current_vis = self.frames[self.frame_i].copy()
            if not self.hud_hidden:
                if self.frame_i in self.annot:
                    if self.annot[self.frame_i].get("not_visible", False):
                        current_vis = crossout(current_vis)
                    elif self.annot[self.frame_i].get("not_visible", False) is None:
                        current_vis = draw_text(current_vis, '???', size=8, thickness=8, pos='center',
                                                color=(0, 0, 255), bg=False, fit_in=True)
                    elif self.annot[self.frame_i].get("x"):
                        current_vis = crosshair(current_vis, (self.annot[self.frame_i]["x"],
                                                              self.annot[self.frame_i]["y"]),
                                                              color = (0,255,0))
                        zoom_y, zoom_x = zoom(current_vis,self.annot[self.frame_i]["y"],self.annot[self.frame_i]["x"])
                        if zoom_x is not None:
                            self.zoom_y = zoom_y
                            self.zoom_x = zoom_x
                    current_vis = draw_text(current_vis, 'key', size=2, thickness=2, pos='tr')
                frame_i_text = f'#{self.frame_i + 1}/{len(self.frames)} '
                bg_color = True
                if self.is_at_allowed_frame():
                    bg_color = (0,0,255)
                current_vis = draw_text(current_vis, frame_i_text + visibility_text, size=2, thickness=2, pos='tl', bg=bg_color)
            imshow("cv: current", current_vis)
            c = cv2.waitKey(20)
            if c == ord('q'):
                self.previous_backup = datetime.datetime.min
                self.previous_online_backup = datetime.datetime.min
                self.write(backup_only=True)
                break
            elif c in [ord('h'), ord('n')]:  # left
                self.frame_i = max(0, self.frame_i - 1)
            elif c in [ord('l'), ord('m')]:  # right
                self.frame_i = min(len(self.frames) - 1, self.frame_i + 1)
            elif c == ord('j'):  # long_left
                self.frame_i = self.jump_to_allowed_frame(-1)
            elif c == ord('k'):  # long_right
                self.frame_i = self.jump_to_allowed_frame(1)
            elif c == ord('+'):  # next query
                self.next_query(+1)
            elif c == ord('-'):  # prev query
                self.next_query(-1)
            elif c == ord('x'):
                self.hud_hidden = not self.hud_hidden
                self.next_query(0)  # update images
            elif c == ord('w'):
                if self.mode == 'create':
                    self.write_task()
                else:
                    self.write()
            elif c == ord('a') and self.mode == 'create':
                self.queries.append(copy(self.queries[self.query_i]))
                self.query_i = len(self.queries) - 1
                self.next_query(0)
            elif c in [0, 82]: # up
                self.move_point(0,-1)
            elif c in [1, 84]: # down
                self.move_point(0,1)
            elif c in [2, 81]: # left
                self.move_point(-1,0)
            elif c in [3, 83]: # right
                self.move_point(1,0)
            elif c >= 0:
                print(f"c: {c}")
    def move_point(self,change_x,change_y):
        on_query_frame = self.frame_i == self.queries[self.query_i]["frame_i"]
        if (self.frame_i in self.annot) and self.annot[self.frame_i].get("x") and (not on_query_frame):
            self.annot[self.frame_i]["y"] += change_y
            self.annot[self.frame_i]["x"] += change_x
    def zoom_handler(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            logger.debug(f"(x, y): {(x, y)}")
            on_query_frame = self.frame_i == self.queries[self.query_i]["frame_i"]
            # on_query_frame = False
            if (on_query_frame and self.mode != 'create'):
                return
            
            self.annot[self.frame_i]["x"] = x + self.zoom_x
            self.annot[self.frame_i]["y"] = y + self.zoom_y
            self.annot[self.frame_i]["not_visible"] = False
    def jump_to_allowed_frame(self, direction):
        #zjisti jestli tam jsou frames
        #nacti jestli jo
        frames = self.task[self.video_i]['frames']
        result = find_next_wanted_frame(frames, self.frame_i, direction)
        
        if result is not None:
            return result
        else:
            return self.frame_i
    def is_at_allowed_frame(self):
        frames = self.task[self.video_i]['frames']
        return self.frame_i in frames
        

        
        

                
        

    @iex
    def handler(self, event, x, y , flags, param):
        y -= 2 #hack kvuli nepresnymu kurzoru
        x -= 1
        if event == cv2.EVENT_LBUTTONDOWN:
            logger.debug(f"(x, y): {(x, y)}")
            on_query_frame = self.frame_i == self.queries[self.query_i]["frame_i"]
            # on_query_frame = False
            if (on_query_frame and self.mode != 'create'):
                return
            self.annot[self.frame_i]["x"] = x
            self.annot[self.frame_i]["y"] = y
            self.annot[self.frame_i]["not_visible"] = False
            if self.mode == 'create':
                query = {'frame_i': self.frame_i,
                         'x': x,
                         'y': y}
                self.queries[self.query_i] = query
                self.next_query(0)
        elif event == cv2.EVENT_RBUTTONDOWN:
            if self.frame_i in self.annot:
                current_notvis = self.annot[self.frame_i].get("not_visible", None)
                if current_notvis is None:  # visibility not sure -> delete annotation
                    del self.annot[self.frame_i]
                elif current_notvis:  # invisible -> not sure
                    self.annot[self.frame_i]["not_visible"] = None
                    if "x" in self.annot[self.frame_i]:  # just in case
                        del self.annot[self.frame_i]["x"]
                    if "y" in self.annot[self.frame_i]:
                        del self.annot[self.frame_i]["y"]
                elif not current_notvis:  # visible with point coords -> invisible
                    self.annot[self.frame_i]["not_visible"] = True
                    if "x" in self.annot[self.frame_i]:
                        del self.annot[self.frame_i]["x"]
                    if "y" in self.annot[self.frame_i]:
                        del self.annot[self.frame_i]["y"]
            else:  # not annotated -> invisible
                self.annot[self.frame_i]["not_visible"] = True
        elif event == cv2.EVENT_MOUSEMOVE:
            self.dragged = True
        elif event == cv2.EVENT_LBUTTONDOWN:
            self.dragged = False

    def write_task(self):
        path = Path('tasks') / 'tasks' / self.task_name / 'task.json'
        with path.open('w') as fout:
            json.dump(self.task, fout, indent=2, sort_keys=True)

    def has_position(self, frame_i):
        return (frame_i in self.annot) and ("x" in self.annot[frame_i]) and ("y" in self.annot[frame_i])

    def is_currently_visible(self):
        visible = None
        for i in range(0, self.frame_i + 1):
            if (i in self.annot) and ("not_visible" in self.annot[i]):
                current_not_vis = self.annot[i]["not_visible"]
                if current_not_vis is None:
                    visible = None
                elif current_not_vis:
                    visible = False
                else:
                    visible = True

        return visible

    @iex
    def next_query(self, delta=1):
        query_i = self.query_i + delta
        video_i = self.video_i
        video_changed = False
        if query_i < 0:
            # prev video
            video_i -= 1
            video_changed = True
            if video_i < 0:
                return
            query_i = len(self.task[video_i]['queries']) - 1
        elif (query_i >= len(self.task[self.video_i]['queries']) and
              len(self.task[self.video_i]['queries']) > 0):
            # next video
            video_changed = True
            video_i += 1
            if video_i >= len(self.task):
                return
            query_i = 0
        
        candidate_queries = self.task[video_i]['queries']
        nonzero_delta = delta if delta != 0 else 1
        while (len(candidate_queries) == 0) or ('x' not in candidate_queries[0]):
            video_i += nonzero_delta
            candidate_queries = self.task[video_i]['queries']

            print ("skipping video without valid query")
            if video_i < 0 or video_i >= len(self.task):
                return

        # load the appropriate query / video
        self.video_i = video_i
        self.query_i = query_i
        if video_changed or self.frames is None:
            self.frames = None
            video_name = self.task[self.video_i]['video']
            video_path = Path('tasks') / 'videos' / video_name
            self.frames = [frame for frame in tqdm.tqdm(io.get_video_frames(video_path),
                                                        desc=f"loading video {video_i+1} ({video_name})",
                                                        total= io.get_video_length(video_path))]
            assert len(self.frames) > 0, "Loaded 0 video frames :("

        self.queries = self.task[self.video_i]['queries']
        if len(self.queries) == 0 and self.mode == 'create':
            self.frame_i = 0
            self.queries.append({"frame_i": self.frame_i,
                                 "not_visible": None})
        query = self.queries[self.query_i]
        # template_vis = to_gray_3ch(self.frames[query["frame_i"]])
        template_vis = self.frames[query["frame_i"]].copy()
        if not self.hud_hidden:
            if not self.mode == 'create':
                template_vis = crosshair(template_vis, (query['x'], query['y']), (0, 0, 255))
            template_str = (f'{shorten_text(self.task[self.video_i]["video"], 12, keep="right")}: '
                            f'q: {self.query_i + 1}/{len(self.queries)} v: {self.video_i + 1}/{len(self.task)} '
                            f'#{query["frame_i"]+1}')
            template_vis = draw_text(template_vis, template_str, size=2, thickness=2, pos='tl')

        imshow("cv: template", template_vis)
        if delta != 0:
            self.frame_i = query["frame_i"]
        current = self.frames[self.frame_i].copy()
        imshow("cv: current", current)
        cv2.setMouseCallback("cv: current", self.handler)
        flags = cv2.WINDOW_NORMAL
        flags = flags | cv2.WINDOW_GUI_NORMAL
        cv2.namedWindow("cv: zoom", flags)
        cv2.setMouseCallback("cv: zoom", self.zoom_handler)

        video_name = self.task[self.video_i]['video']
        if video_name not in self.all_annotations:
            self.all_annotations[video_name] = {}
        if self.query_i not in self.all_annotations[video_name]:
            self.all_annotations[video_name][query_i] = defaultdict(dict)
            self.all_annotations[video_name][query_i][query["frame_i"]] = {
                "x": query.get("x", None),
                "y": query.get("y", None),
                "not_visible": query.get("not_visible", False)}
        self.annot = self.all_annotations[video_name][query_i]

    def write(self, backup_only=False):
        now = datetime.datetime.now()
        seconds_since_last_backup = (now - self.previous_backup).total_seconds()
        seconds_since_last_online_backup = (now - self.previous_online_backup).total_seconds()

        if seconds_since_last_backup > 15 * 60:
            self.previous_backup = now
            stamp = now.strftime("%Y-%m-%d_%H-%M-%S")
            backup_dir = Path('tasks') / 'tasks' / self.task_name / 'backups'
            backup_dir.mkdir(parents=True, exist_ok=True)
            backup_path = str(backup_dir / f'{stamp}_annot.pklz')
            with gzip.open(backup_path, 'wb') as fout:
                pickle.dump(self.all_annotations, fout)

            hostname = socket.gethostname()
            no_backup_names = ['jondell']
            # no_backup_names = []
            if seconds_since_last_online_backup > 60 * 60 and hostname not in no_backup_names:
                try:
                    io.upload(backup_path, self.task_name, self.upload_url)
                    self.previous_online_backup = now
                except Exception:
                    logger.exception("Backup upload failed")

        if not backup_only:
            save_path = str(Path('tasks') / 'tasks' / self.task_name / 'annot.pklz')
            with gzip.open(save_path, 'wb') as fout:
                pickle.dump(self.all_annotations, fout)
def find_next_wanted_frame(frames, current_frame, direction):
        current = 0
        if direction == 1:
            current = float('inf')
            for frame in frames:
                if frame > current_frame and frame < current:
                    current = frame
        else:
            current = float('-inf')
            for frame in frames:
                if frame < current_frame and frame > current:
                    current = frame

        if current not in [float('inf'), float('-inf')]:
            return current
        else:
            return None

def to_gray_3ch(img):
    return cv2.cvtColor(cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),
                        cv2.COLOR_GRAY2BGR)


def crosshair(img, pos, color=(0, 0, 255), thickness=1, ctype='circle'):
    canvas = img.copy()
    H, W = img.shape[:2]
    cv2.line(canvas, (0, pos[1]), (W, pos[1]), color, thickness)
    cv2.line(canvas, (pos[0], 0), (pos[0], H), color, thickness)
    r = 50
    if ctype == 'circle':
        cv2.circle(canvas, (pos[0], pos[1]), r, color, 4)
    elif ctype == 'square':
        cv2.rectangle(canvas,
                      (pos[0] - r, pos[1] - r),
                      (pos[0] + r, pos[1] + r),
                      color, thickness=4)
    elif ctype == 'square_rot':
        cv2.line(canvas, (pos[0] + r, pos[1] + 0), (pos[0] - 0, pos[1] + r), color, thickness=4)
        cv2.line(canvas, (pos[0] - 0, pos[1] + r), (pos[0] - r, pos[1] + 0), color, thickness=4)
        cv2.line(canvas, (pos[0] - r, pos[1] + 0), (pos[0] - 0, pos[1] - r), color, thickness=4)
        cv2.line(canvas, (pos[0] - 0, pos[1] - r), (pos[0] + r, pos[1] + 0), color, thickness=4)
    elif ctype == 'none':
        pass
    else:
        raise NotImplementedError('This type of crosshair is not implemented')

    return canvas




def crossout(img, color=(0, 0, 255), thickness=5):
    canvas = img.copy()
    H, W = img.shape[:2]
    cv2.line(canvas, (0, 0), (W, H), color, thickness)
    cv2.line(canvas, (0, H), (W, 0), color, thickness)
    return canvas


def imshow(win_name, img, scalable=True):
    flags = cv2.WINDOW_AUTOSIZE
    if scalable:
        flags = cv2.WINDOW_NORMAL

    flags = flags | cv2.WINDOW_GUI_NORMAL
    cv2.namedWindow(win_name, flags)
    cv2.imshow(win_name, img)


def draw_text(img, text, size=3, color=(255, 255, 255),
              pos='bl', thickness=3,
              bg=True, bg_alpha=0.5,
              fit_in=True):

    canvas = img.copy()
    text_margin = 5
    font = cv2.FONT_HERSHEY_SIMPLEX

    if isinstance(text, (list, tuple)):
        texts = text
    else:
        texts = [text]

    if isinstance(color, (list, tuple)) and isinstance(color[0], int):
        colors = [color]
        bg_color = (255 - color[0], 255 - color[1], 255 - color[2])
    elif isinstance(color, (list, tuple)) and len(color) == len(texts):
        colors = color
        bg_color = (0, 0, 0)
    else:
        raise ValueError("weird color input")
    if isinstance(bg, (list, tuple)) and isinstance(bg[0], int):
        bg_color = bg

    while True:
        text_size, text_baseline = cv2.getTextSize(''.join(texts), font, size, thickness)
        text_W, text_H = text_size
        if fit_in:
            if (text_W > canvas.shape[1] or text_H > canvas.shape[0]) and size > 1:
                # try again with smaller size
                size = max(1, size - 1)
                thickness = max(1, thickness - 1)
            else:
                break  # found a fitting size, or cannot shrink any more
        else:
            break
    if pos == 'bl':
        text_bl = (text_margin, canvas.shape[0] - (text_margin + text_baseline))
    elif pos == 'tr':
        text_bl = (canvas.shape[1] - (text_margin + text_W),
                   text_margin + text_H)
    elif pos == 'tl':
        text_bl = (text_margin,
                   text_margin + text_H)
    elif pos == 'br':
        text_bl = (canvas.shape[1] - (text_margin + text_W),
                   canvas.shape[0] - (text_margin + text_baseline))
    elif pos == 'center':
        text_bl = (canvas.shape[1] // 2 - text_W // 2,
                   canvas.shape[0] // 2 + text_H // 2)
    else:
        text_bl = pos

    if bg:
        bg_margin = 2
        bg_canvas = canvas.copy()
        cv2.rectangle(bg_canvas,
                      (text_bl[0] - bg_margin, text_bl[1] + bg_margin + text_baseline),
                      (text_bl[0] + text_W + bg_margin, text_bl[1] - text_H - bg_margin),
                      bg_color, thickness=-1)
        canvas = cv2.addWeighted(bg_canvas, bg_alpha, canvas, (1 - bg_alpha), 0)

    bl = text_bl
    for text_i, text in enumerate(texts):
        c = colors[text_i % len(colors)]
        canvas = cv2.putText(canvas, text,
                             bl, font,
                             size, c, thickness, cv2.LINE_AA)
        bl = (bl[0] + cv2.getTextSize(text, font, size, thickness)[0][0], bl[1])
    return canvas


def shorten_text(text, max_len, keep='right'):
    if len(text) <= max_len:
        return text

    margin = 3 if max_len > 8 else 0
    if keep == 'right':
        return "." * margin + text[-(max_len - margin):]
    elif keep == 'left':
        return text[:(max_len - margin)] + "." * margin
    else:
        raise ValueError("Invalid 'keep' value")


class OverlayBackground(object):
    """It would be so nice to have Lisp macros and not to have to deal with Python weakness...

    It would be so nice!

    (with-overlay (img_a img_b :alpha 0.5)
      ... operations on img_a ...
      ... operations on img_b ...
    )

    img_a would now be a 1:1 mix between original img_a and img_a after the operations, same for img_b
    """

    def __init__(self, *background_images):
        self.backgrounds_copy = [img.copy() for img in background_images]

    def overlay(self, *overlay_images, alpha=0.5):
        return [cv2.addWeighted(overlay, alpha, background, (1 - alpha), 0)
                for overlay, background in zip(overlay_images, self.backgrounds_copy)]
    


def zoom(img, y, x, c=100):
    pad = c
    y_low = y - c + pad
    y_high = y + c + pad
    x_low = x - c + pad
    x_high = x + c + pad
    padded = cv2.copyMakeBorder(img,pad,pad,pad,pad,cv2.BORDER_CONSTANT,value = (0,0,255))
    try:
        zum = padded[y_low: y_high, x_low: x_high]    
        imshow("cv: zoom" , zum)
        return y_low - pad, x_low-pad

    except Exception:
        return None, None
