import zipfile
import functools
import pathlib
import shutil
import requests
from tqdm.auto import tqdm
from pathlib import Path
import os
import cv2


def download(url, filename):
    """ from https://stackoverflow.com/a/63831344/1705970 """

    r = requests.get(url, stream=True, allow_redirects=True)
    if r.status_code != 200:
        r.raise_for_status()  # Will only raise for 4xx codes, so...
        raise RuntimeError(f"Request to {url} returned status code {r.status_code}")
    file_size = int(r.headers.get('Content-Length', 0))

    path = pathlib.Path(filename).expanduser().resolve()
    path.parent.mkdir(parents=True, exist_ok=True)

    desc = "(Unknown total file size)" if file_size == 0 else ""
    r.raw.read = functools.partial(r.raw.read, decode_content=True)  # Decompress if needed
    with tqdm.wrapattr(r.raw, "read", total=file_size, desc=desc) as r_raw:
        with path.open("wb") as f:
            shutil.copyfileobj(r_raw, f)

    return path


def unzip(zip_path, target_dir):
    target_dir.mkdir(parents=True, exist_ok=True)
    with zipfile.ZipFile(zip_path, "r") as zip_ref:
        zip_ref.extractall(target_dir)


class GeneralVideoCapture(object):
    """A cv2.VideoCapture replacement, that can also read images in a directory"""

    def __init__(self, path, reverse=False):
        images = Path(path).is_dir()
        self.image_inputs = images
        if images:
            self.path = path
            self.images = sorted([f for f in next(os.walk(path))[2]
                                  if os.path.splitext(f)[1].lower() in ['.jpg', '.png', '.jpeg']])
            if reverse:
                self.images = self.images[::-1]
            self.i = 0
        else:
            self.cap = cv2.VideoCapture(str(path))

    def read(self):
        if self.image_inputs:
            if self.i >= len(self.images):
                return False, None
            img_path = os.path.join(self.path,
                                    self.images[self.i])
            self.frame_src = self.images[self.i]
            img = cv2.imread(img_path)
            self.i += 1
            return True, img
        else:
            return self.cap.read()

    def release(self):
        if self.image_inputs:
            return None
        else:
            return self.cap.release()

    def __len__(self):
        if self.image_inputs:
            return len(self.images)


def get_video_frames(path):
    cap = GeneralVideoCapture(path)
    while True:
        success, frame = cap.read()
        if not success or frame is None:
            return None
        yield frame


def get_video_length(path):
    cap = GeneralVideoCapture(path)
    if len(cap) is not None:
        return len(cap)
    cap.release()
    
    N = 0
    for frame in get_video_frames(path):
        N += 1
    return N


def download_task_videos(task, base_url, dst_dir):
    dst_dir.mkdir(parents=True, exist_ok=True)

    to_down = []
    for subtask in task:
        video_name = subtask['video']
        if not (dst_dir / video_name).exists():
            to_down.append(video_name)

    if len(to_down) > 0:
        for video_name in tqdm(to_down, desc="Video download", position=0):
            video_url = f'{base_url}/zipped_videos/{video_name}.zip'
            local_path = dst_dir / f'{video_name}.zip'
            download(video_url, local_path)
            unzip(local_path, dst_dir)
            local_path.unlink(missing_ok=True)  # cleanup zip


def upload(backup_path, task_name, upload_url):
    with open(backup_path, 'rb') as fin:
        response = requests.post(upload_url,
                                 files={"fileToUpload": fin},
                                 data={"submit": True,
                                       "task_name": task_name})
        if not response.ok:
            print(response.text)
