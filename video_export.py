# -*- origami-fold-style: triple-braces; coding: utf-8; -*-
import sys
import argparse
from pathlib import Path
import cv2
import numpy as np
import pickle
import gzip
from vpta import io
from vpta.vis import crosshair
import json
from tqdm import tqdm
from ipdb import iex

import logging
logger = logging.getLogger(__name__)


def parse_arguments():
    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', '--verbose', help='', action='store_true')
    parser.add_argument('task', help='task name')
    parser.add_argument('--annot', help='path to annotations', type=Path)
    parser.add_argument('--cont', help='continue mode - skip already rendered videos', action='store_true')
    parser.add_argument('--vis', help='show preview', action='store_true')

    args = parser.parse_args()

    format = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    lvl = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=lvl, format=format)
    logging.getLogger("asyncio").setLevel(logging.WARNING)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    return args


def run(args):
    annot_path = args.annot
    if annot_path is None:
        annot_path = f"tasks/tasks/{args.task}/annot.pklz"

    with open(f"tasks/tasks/{args.task}/task.json",'r') as fin:
        task = json.load(fin)

    with gzip.open(annot_path, 'rb') as fin:
        all_annotations = pickle.load(fin)

    for video_i in tqdm(range(len(task)),desc="video"):
        video_name = task[video_i]['video']
        # if video_i in [20, 22]:
        #     print(f'skipping video {video_i} - {video_name}')
        #     continue
        if video_name not in all_annotations:
            continue
        print(f'rendering video {video_i} - {video_name}')
        images = None
        allowed_frames = task[video_i].get('frames')
        for query_i in range(len(all_annotations[video_name])):
            annotations = all_annotations[video_name][query_i]
            path = f"video_export/{video_name}_v{video_i:03d}_q{query_i:02d}.mp4"
            if Path(path).exists() and args.cont:
                print(f'skipping {path} - already rendered')
                continue
            if images is None:
                images = [x for x in io.get_video_frames('tasks/videos/' + video_name)]
            export_video(images, annotations, path, allowed_frames=allowed_frames, vis=args.vis)

    cv2.destroyAllWindows()
    return 0

@iex
def main():
    args = parse_arguments()
    return run(args)

def zoom(img, y, x, c=100):
    pad = c
    y_low = y - c+pad
    y_high = y + c+pad
    x_low = x - c + pad
    x_high = x + c +pad
    padded = cv2.copyMakeBorder(img,pad,pad,pad,pad,cv2.BORDER_CONSTANT,value = (0,0,0))
    try:
        zum = padded[y_low: y_high, x_low: x_high]
        return zum

    except Exception:
        return None


def export_video(images, annotations, output_path, zoom_factor=2, vyrez_size=(100, 100),
                 allowed_frames=None, vis=False):
    size = [zoom_factor * vyrez_size[0], 
            zoom_factor * vyrez_size[1]]
    size[0] += 1 - size[0] % 2
    size[1] += 1 - size[1] % 2
    assert size[0] % 2 == 1
    assert size[1] % 2 == 1
    pad = 2
    fourcc = cv2.VideoWriter_fourcc(*'avc1')
    video_shape = (images[0].shape[1] + size[1] + 2 * pad,
                   max(images[0].shape[0], size[0] + 2 * pad))
    out = cv2.VideoWriter(output_path, fourcc, 25.0, video_shape)
    for frame_i in range(len(images)):
        image = images[frame_i].copy()
        current = annotations[frame_i] 
        if (allowed_frames is None) or (frame_i in allowed_frames):
            if 'x' in current:
                x = max(current['x'], 0)
                y = max(current['y'], 0)
                vyrez = zoom(image, y, x)
                zvetseny_vyrez = cv2.resize(vyrez ,size)
                zvetseny_vyrez = crosshair(
                    zvetseny_vyrez,
                    (size[1] // 2, size[0] // 2),
                    ctype='none')
                                           
                # cv2.imshow("zoom", zvetseny_vyrez)
                cv2.circle(image, (x,y), 10, (0,0,255), -1)
            else:
                zvetseny_vyrez = np.zeros((size[0],size[1],3), dtype=np.uint8)
        vyrez_border = cv2.copyMakeBorder(zvetseny_vyrez, pad, pad, pad, pad,
                                          cv2.BORDER_CONSTANT,value=(255,255,255))
        canvas = np.zeros((max(image.shape[0], vyrez_border.shape[0]),
                           image.shape[1] + vyrez_border.shape[1],
                           3),
                          dtype=np.uint8)
        assert canvas.shape[0] == video_shape[1]
        assert canvas.shape[1] == video_shape[0]
        canvas[:image.shape[0], :image.shape[1], :] = image
        canvas[0:vyrez_border.shape[0], image.shape[1]:image.shape[1] + vyrez_border.shape[1], :] = vyrez_border

        # image[0:vyrez_border.shape[0], 0:vyrez_border.shape[1],:] = vyrez_border
        out.write(canvas)
        if vis:
            cv2.imshow("Loaded Image", canvas)
            c = cv2.waitKey(5)
            if c == ord('q'):
                sys.exit(1)
    out.release()


if __name__ == '__main__':
    results = main()

